set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)
cmake_minimum_required(VERSION 3.24)

# specify cross-compilers and tools
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# project settings
project(Updater C ASM)
set(CMAKE_C_STANDARD 11)

add_compile_options(-mcpu=cortex-m4 -mthumb -mthumb-interwork)
add_compile_options(-ffunction-sections -fdata-sections -fno-common -fmessage-length=0)

# Enable assembler files preprocessing
add_compile_options($<$<COMPILE_LANGUAGE:ASM>:-x$<SEMICOLON>assembler-with-cpp>)

# To reduce binary file output size.
add_compile_options(-fdata-sections)
add_compile_options(-ffunction-sections)
add_compile_options(-fmessage-length=0)
add_compile_options(-fno-common)
add_compile_options(-fno-exceptions)
add_compile_options(--specs=nano.specs)

add_compile_options(-fdiagnostics-color=always)

add_compile_options(-Oz -g) # Force max size optimization
set(MCU_DIR ${CMAKE_SOURCE_DIR}/F405)

include_directories(
        CEP
        ${MCU_DIR}/Core/Inc
        ${MCU_DIR}/Drivers/CMSIS/Device/ST/STM32F4xx/Include
        ${MCU_DIR}/Drivers/CMSIS/Include
        ${MCU_DIR}/Drivers/STM32F4xx_HAL_Driver/Inc
        ${MCU_DIR}/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy
        ${MCU_DIR}/FATFS/App
        ${MCU_DIR}/FATFS/Target
        ${MCU_DIR}/Middlewares/Third_Party/FatFs/src
)

add_definitions(-DUSE_HAL_DRIVER -DDEBUG -DSTM32F405xx)

file(GLOB_RECURSE SOURCES
        "${MCU_DIR}/Core/*.*"
        "${MCU_DIR}/Drivers/*.*"
        "${MCU_DIR}/FATFS/*.*"
        "${MCU_DIR}/Middlewares/*.*"
        "CEP/*.*"
)

set(LINKER_SCRIPT ${MCU_DIR}/STM32F405RGTX_FLASH.ld)

add_link_options(-Wl,-gc-sections,--print-memory-usage,-Map=${PROJECT_BINARY_DIR}/${PROJECT_NAME}.map)
add_link_options(-mcpu=cortex-m4 -mthumb -mthumb-interwork)
add_link_options(--specs=nano.specs)
add_link_options(-u _printf_float)
add_link_options(-Wl,--no-warn-rwx-segments) # Disable GCC 12.2 new warning for RWX segments
add_link_options(-T ${LINKER_SCRIPT})
add_link_options(-Wl,--build-id) #Bloaty

add_executable(${PROJECT_NAME}.elf ${SOURCES} ${LINKER_SCRIPT})

set(HEX_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.hex)
set(BIN_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.bin)

add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}
        COMMENT "Building ${HEX_FILE}"
        COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}
        COMMENT "Building ${BIN_FILE}")
