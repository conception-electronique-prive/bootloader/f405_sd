#include <main.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include "gpio.h"
#include "fatfs.h"
#include "usart.h"
#include "spi.h"
#include "bootloader.h"

#define LED_STATUS_OFF() HAL_GPIO_WritePin(LED_STATUS_GPIO_Port, LED_STATUS_Pin, GPIO_PIN_RESET)
#define LED_STATUS_TG()  HAL_GPIO_TogglePin(LED_STATUS_GPIO_Port, LED_STATUS_Pin)

#define VERSION "2.0.1"
#define COMM    "SD"
#define BAR     "====================="

#define KEEP_FILE         "keep"
#define FIRMWARE_FOLDER   "stm32"
#define FIRMWARE_FILE     "firmware.bin"
#define FIRMWARE_FILEPATH FIRMWARE_FOLDER "/" FIRMWARE_FILE

void init_hal(void);
void print_info(void);
void mount_sdcard(void);
bool check_must_update(void);
void check_setup_integrity(void);
void erase_previous(void);
void flash_firmware(void);
void check_firmware(void);
void cleanup(void);
void eject_sdcard(void);
void deinit_hal(void);
void jump_to_app(void);
void unreachable(void);

void printnr(const char* tag, const char* fmt, ...) {
    printf("[%s]: ", tag);
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf("\r");
}
void print(const char* tag, const char* fmt, ...) {
    printf("[%s]: ", tag);
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf("\r\n");
}
void clrl() {
    printf("\r%60s\n", "");
}

int main(void) {
    init_hal();
    print_info();
    mount_sdcard();
    if (check_must_update()) {
        print("APP", "Must update");
        check_setup_integrity();
        erase_previous();
        flash_firmware();
        check_firmware();
        cleanup();
    } else {
        print("APP", "No file to flash");
    }
    eject_sdcard();
    jump_to_app();
    unreachable();
}

void init_hal(void) {
    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
    MX_FATFS_Init();
    MX_USART_UART_Init();
    MX_SPI_Init();
}

void print_info(void) {
    printf("\033c");
    printf("CEP BOOTLOADER\r\n");
    printf("[COMM ]: " COMM "\r\n");
    printf("[VER  ]: " VERSION "\r\n");
    printf(BAR "\r\n");
}

void mount_sdcard(void) {
    printnr("SD", "Mounting");
    FRESULT fr = f_mount(&USERFatFS, (TCHAR const*)USERPath, 1);
    if (fr != FR_OK) {
        print("SD", "Cannot be mounted. FatFs error code: %u", fr);
        Error_Handler();
    }
    clrl();
    print("SD", "Mounted");
}

bool check_must_update(void) {
    FRESULT fr;
    FILINFO fi;
    fr = f_stat(FIRMWARE_FOLDER, &fi);
    if (fr != FR_OK && fr != FR_NO_FILE) {
        print("APP", "Failed to check if update required. FatFs error: %u", fr);
        Error_Handler();
    }
    if (fr == FR_NO_FILE) {
        return false;
    }

    fr = f_stat(FIRMWARE_FILEPATH, &fi);
    if (fr != FR_OK && fr != FR_NO_FILE) {
        print("APP", "Failed to check if update required. FatFs error: %u", fr);
        Error_Handler();
    }

    return fr == FR_OK;
}

void check_setup_integrity(void) {
    uint32_t ex_devid = 0x413;    // RM0090-r.19 38.6.1
    uint32_t devid    = HAL_GetDEVID();
    if (devid != ex_devid) {
        print("ERROR", "Invalid device ID. Expected %lx, got %lx", ex_devid, devid);
        Error_Handler();
    }
}

void erase_previous(void) {
    printnr("ERAZ", "Running");
    uint8_t status = Bootloader_Erase();
    if (status != BL_OK) {
        print("ERAZ", "Failed: %u", status);
        Error_Handler();
    }
    clrl();
    print("ERAZ", "Done");
}

void flash_firmware(void) {
    FIL     file;
    FRESULT fr;
    UINT    num;

    uint8_t  status;
    uint32_t cntr = 0;
    uint64_t data;
    size_t   size;

    fr = f_open(&file, FIRMWARE_FILEPATH, FA_READ);
    if (fr != FR_OK) {
        print("FILE", "Failed to open bootloader file for flash. FatFs error: %u", fr);
        Error_Handler();
    }
    size = f_size(&file);

    printnr("PROG", "Starting");
    Bootloader_FlashBegin();
    do {
        data = 0xFFFFFFFFFFFFFFFF;
        fr   = f_read(&file, &data, 8, &num);
        if (num) {
            status = Bootloader_FlashNext(data);
            if (status == BL_OK) {
                cntr++;
            } else {
                print("PROG", "Error at: %lu (0x%lx)", (cntr * 8), (cntr * 8));
                Error_Handler();
            }
        }
        if (cntr % 256 == 0) {
            LED_STATUS_TG();
            printnr("PROG", "%2lu%% [%6lu/%6u]", cntr * 8 * 100 / size, cntr * 8, size);
        }
    } while ((fr == FR_OK) && (num > 0));

    Bootloader_FlashEnd();
    LED_STATUS_OFF();
    clrl();
    print("PROG", "Flashed %u bytes", size);

    fr = f_close(&file);
    if (fr != FR_OK) {
        print("FILE", "Failed to close file for flash. FatFs error: %u", fr);
        Error_Handler();
    }
}

void check_firmware(void) {
    FIL     file;
    FRESULT fr;
    UINT    num;

    uint32_t addr = APP_ADDRESS;
    uint32_t cntr = 0;
    uint64_t data;
    size_t   size;

    fr = f_open(&file, FIRMWARE_FILEPATH, FA_READ);
    if (fr != FR_OK) {
        print("FILE", "Failed to open bootloader file for check. FatFs error: %u", fr);
        Error_Handler();
    }
    size = f_size(&file);

    printnr("CHCK", "Starting");
    do {
        data = 0xFFFFFFFFFFFFFFFF;
        fr   = f_read(&USERFile, &data, 4, &num);
        if (num) {
            if (*(uint32_t*)addr == (uint32_t)data) {
                addr += 4;
                cntr++;
            } else {
                clrl();
                print("CHCK", "Error at: %lu byte", cntr * 4);
                Error_Handler();
            }
        }
        if (cntr % 256 == 0) {
            LED_STATUS_TG();
            printnr("CHCK", "%2lu%% [%6lu/%6u]", cntr * 4 * 100 / size, cntr * 4, size);
        }
    } while ((fr == FR_OK) && (num > 0));
    clrl();
    print("CHCK", "Passed");
    LED_STATUS_OFF();

    fr = f_close(&file);
    if (fr != FR_OK) {
        print("FILE", "Failed to close file for check. FatFs error: %u", fr);
        Error_Handler();
    }
}

void cleanup(void) {
    FRESULT fr;
    FILINFO fi;
    fr = f_stat(KEEP_FILE, &fi);
    if (fr == FR_OK) {
        print("CLEAN", "Keep file present, keeping files");
        return;
    }

    fr = f_unlink(FIRMWARE_FILEPATH);
    if (fr != FR_OK) {
        print("CLEAN", "Failed to erase firmware file. FatFs error: %u", fr);
        Error_Handler();
    }

    f_unlink(FIRMWARE_FOLDER);
}

void eject_sdcard(void) {
    f_mount(NULL, (TCHAR const*)USERPath, 0);
}

void deinit_hal(void) {
    MX_SPI_DeInit();
    MX_USART_UART_DeInit();
    MX_FATFS_DeInit();
    MX_GPIO_DeInit();
}

void jump_to_app(void) {
    if (Bootloader_CheckForApplication() == BL_OK) {
        printf("Jumping to application\r\n");
        printf(BAR "\r\n");
        LED_STATUS_OFF();
        deinit_hal();
        Bootloader_JumpToApplication();
    }
}

void unreachable(void) {
    print("APP", "No application in flash");
    Error_Handler();
}
