# Bootloader for STM32F405

Original project: https://github.com/akospasztor/stm32-bootloader

Modified by CEP to support STM32F405

## Behavior

1. Initialize peripherals (HAL, Clock, GPIO, SPI, UART, FATFS)
2. Print Bootloader Information
3. Mount SD Card
4. On presence of firmware file on the SD card
    1. Erase Application space on Flash memory
    2. Write firmware file content on Flash memory
    3. Verify rightness of the written content
    4. Erase firmware file from SD card
    5. Any error in previous steps cancel the flashing procedure
5. Unmount SD Card
6. De-Initialize peripherals (HAL, Clock, GPIO, SPI, UART, FATFS)
7. Jump to application

## Requirements

### Binary

The firmware file must be named `firmware.bin`
and located in a folder named `stm32`
itself placed at the root of the SD card

```
.
└── stm32/
    └── firmware.bin
```

### Application

On the application code (not bootloader)

In `STM32F405RGTX_FLASH.ld`, you must update the memory definition to

```
/* Memories definition */
MEMORY
{
  RAM    (xrw)    : ORIGIN = 0x20000000,   LENGTH = 160K
  FLASH    (rx)    : ORIGIN = 0x8008000,   LENGTH = 480K
}
``` 
